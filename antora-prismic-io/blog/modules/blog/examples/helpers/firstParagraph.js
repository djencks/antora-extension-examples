'use strict'

module.exports = (slices) => {
  for (const slice of slices) {
    if (slice.slice_type == "text") {
      for (const block of slice.primary.text) {
        if (block.type == "paragraph") {
          return block
        }
      }
    }
  }
}
