#!/bin/sh

ROOT=`pwd`

for p in antora-* antora-pdf/simple-pdfs
do
  echo $p
  cd $p
  npm run clean-build
  cd $ROOT
  echo $p done
done

echo done with all
